package com.az.flickrclient.domain

import com.az.flickrclient.data.response.PublicFeedItemJsonResponse
import java.text.DateFormat
import java.text.ParseException
import java.util.Date

/**
 * Created by Michal S. on 05.12.2017.
 * Converters for json data strings to final structure
 */
//TODO converters


fun PublicFeedItemJsonResponse.getPublishedDate(): Date? {
    val datePattern = "yyyy-MM-dd'T'HH:mm:ss'Z'"
    val dateFormatter: DateFormat = java.text.SimpleDateFormat(datePattern, java.util.Locale.getDefault())
    published?.let {
        return try {
            dateFormatter.parse(it)
        } catch (e: ParseException) {
            null
        }
    }
    return null
}

//fun PublicFeedItemJsonResponse.getTags():List<String> {
//    return if (tags.trim().length==0) ArrayList<String>() else tags.split(" ")
//
//}

fun PublicFeedItemJsonResponse.getTags():String{
    return tags
}