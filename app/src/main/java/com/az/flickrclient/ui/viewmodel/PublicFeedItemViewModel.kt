package com.az.flickrclient.ui.viewmodel

import android.databinding.ObservableField
import com.az.flickrclient.domain.PublicFeedItem

/**
 * Created by Michal S. on 05.12.2017.
 */
class PublicFeedItemViewModel {
    val publicFeedItem = ObservableField<PublicFeedItem>()

    fun setItem(publicFeedItem: PublicFeedItem) {
        this.publicFeedItem.set(publicFeedItem)
    }
}