package com.az.flickrclient.data.response

/**
 * Created by Michal S. on 04.12.2017.
 * Media Item POJO for json format response
 */
data class MediaJsonResponse(val m: String)