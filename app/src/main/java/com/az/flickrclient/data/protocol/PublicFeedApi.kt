package com.az.flickrclient.data.protocol


import com.az.flickrclient.data.response.PublicFeedJsonResponse
import io.reactivex.Single
import retrofit2.http.GET

/**
 * Created by Michal S. on 05.12.2017.
 */
interface PublicFeedApi {
    @get:GET("feeds/photos_public.gne?format=json&nojsoncallback=1")
    val publicFeed: Single<PublicFeedJsonResponse>
}