package com.az.flickrclient.data.response

/**
 * Created by Michal S. on 04.12.2017.
 * Public Feed structure for json array
 */
data class PublicFeedJsonResponse(val items: List<PublicFeedItemJsonResponse>)